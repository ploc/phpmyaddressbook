<a name="2.8"></a>
# [2.8](https://git.framasoft.org/ploc/phpmyaddressbook/compare/2.7...2.8) (2022-02-15)


### Features

- manage focus by using anchor
- favicon


### Improvements

- responsive theme
- fpdf updated to version 1.84
- pdf label updated to version 1.6



<a name="2.7"></a>
# [2.7](https://git.framasoft.org/ploc/phpmyaddressbook/compare/2.6...2.7) (2019-05-25)


### Improvements

- mailings are sent in bcc
- fpdf updated to version 1.81
- pdf label updated to version 1.6
- documentation converted to markdown



<a name="2.6"></a>
# [2.6](https://git.framasoft.org/ploc/phpmyaddressbook/compare/2.5...2.6) (2017-06-02)


### Features

- phone numbers have a link to call and to send a text message
- a viewport meta tag to enhance display on mobile


### Improvements

- error pages return an error http status code
- no image embedded in css to comply content security policy


### Bug Fixes

- icons are from IcoMoon and in svg format



<a name="2.5"></a>
# [2.5](https://git.framasoft.org/ploc/phpmyaddressbook/compare/2.4...2.5) (2016-09-12)


### Features

- vcard v2 export module for gigaset



<a name="2.4"></a>
# [2.4](https://git.framasoft.org/ploc/phpmyaddressbook/compare/2.3...2.4) (2015-06-27)


### Features

- tags are exported to vcard


### Improvements

- fpdf updated to version 1.7
- various improvements


### Bug Fixes

- parameters checks secured



<a name="2.3"></a>
# [2.3](https://git.framasoft.org/ploc/phpmyaddressbook/compare/2.2...2.3) (2012-12-12)


### Features

- html5 email, tel, url and search input types
- html5 autofocus attribute
- html5 placeholder attribute
- html5 required attribute
- html5 pattern attribute
- css images included in css file


### Improvements

- html5 template
- various improvements
- suggested value removed



<a name="2.2"></a>
# [2.2](https://git.framasoft.org/ploc/phpmyaddressbook/compare/2.1...2.2) (2011-08-01)


### Features

- click-to-call shortcut
- modular theme management


### Improvements

- icons



<a name="2.1"></a>
# [2.1](https://git.framasoft.org/ploc/phpmyaddressbook/compare/2.0...2.1) (2011-05-16)


### Features

- log file when sending a mailing


### Improvements

- various improvements


### Bug Fixes

- parameters checks secured



<a name="2.0"></a>
# [2.0](https://git.framasoft.org/ploc/phpmyaddressbook/compare/1.9...2.0) (2010-04-07)


### Features

- duplicate feature
- records fields display can be organized by groups
- vcard export module
- label export module
- text export module


### Improvements

- exception error management
- search feature automatically displays first found record
- tableless template
- parameters checks secured
- various improvements


### Bug Fixes

- gettext configuration is charset compliant
- delete feature works with old version of sqlite library
- sim export module set to gsm 7-bit charset



<a name="1.9"></a>
# [1.9](https://git.framasoft.org/ploc/phpmyaddressbook/compare/1.8...1.9) (2009-02-05)


### Features

- csv import module
- sqlite support
- database access uses pdo
- sql statements are sql ansi compliant
- css filename can be set in configuration files
- zimbra export module
- a manual


### Improvements

- csv export module escapes special characters
- mab export module escapes special characters
- mass-mailer is integrated to the interface
- installation is integrated to the interface
- phpMyAddressbook updated to php5
- fpdf updated to version 1.6
- compatible with notice and strict error reporting level
- various improvements


### Bug Fixes

- numeric values are correctly handled
- search feature is always consistent
- mab export module always generates a valid file



<a name="1.8"></a>
# [1.8](https://git.framasoft.org/ploc/phpmyaddressbook/compare/1.7...1.8) (2008-04-22)


### Features

- mass-mailer in order to validate each contact informations
- mab export module
- server side file export


### Improvements

- various improvements


### Bug Fixes

- email, ldif and sim export modules filter null email contacts
- ldif export module is correctly handling name and firstname
- sim export charset set to an unknown but working charset



<a name="1.7"></a>
# [1.7](https://git.framasoft.org/ploc/phpmyaddressbook/compare/1.6...1.7) (2007-11-07)


### Features

- tag feature
- tag shortcuts


### Improvements

- export feature is filter capable
- shortcuts can be enabled or disabled
- init.inc.php settings can be overridden in user.cfg.inc.php file
- csv export module format is simpler
- switch to GPLv3


### Bug Fixes

- update statement is correctly handling numeric data
- contacts with html special characters are correctly handled



<a name="1.6"></a>
# [1.6](https://git.framasoft.org/ploc/phpmyaddressbook/compare/1.5...1.6) (2007-02-28)


### Features

- .htaccess authentication example file


### Improvements

- gettext activation check
- database connection function
- configuration files
- various improvements


### Bug Fixes

- install script is connecting the database



<a name="1.5"></a>
# [1.5](https://git.framasoft.org/ploc/phpmyaddressbook/compare/1.4...1.5) (2007-01-18)


### Improvements

- icons (from Tango Desktop Project)
- default charset set to utf8
- template is using xhtml 1.1
- sql export module is more flexible


### Bug Fixes

- language setting is correctly set in xhtml template



<a name="1.4"></a>
# [1.4](https://git.framasoft.org/ploc/phpmyaddressbook/compare/1.3...1.4) (2006-03-06)


### Improvements

- compatible with register_globals php parameter set to off
- all export filenames are timestamped
- parameters checks are secured
- generic export module


### Bug Fixes

- data are correctly exported to the pdf document



<a name="1.3"></a>
# [1.3](https://git.framasoft.org/ploc/phpmyaddressbook/compare/1.2...1.3) (2005-08-17)


### Improvements

- css is slightly modified
- parameters checks are slightly modified


### Bug Fixes

- parameters checks are working
- export filenames are based on the table name



<a name="1.2"></a>
# [1.2](https://git.framasoft.org/ploc/phpmyaddressbook/compare/1.1...1.2) (2005-06-05)


### Features

- sim card export module
- internationalization using gettext
- phpMyAddressbook can handle unlimited number of fields
- each field has a default value and a suggested value


### Improvements

- all export filenames are timestamped
- using fpdf153
- javascript confirm script removed



<a name="1.1"></a>
# [1.1](https://git.framasoft.org/ploc/phpmyaddressbook/compare/1.0...1.1) (2004-10-12)


### Features

- csv export module
- secured data handling


### Improvements

- full revision of the code (less code means less bug)
- redesigned interface is simpler and more efficient
- template is xhtml strict valid and uses css
- all icons are in png format
- icons
- using fpdf152



<a name="1.0"></a>
# [1.0](https://git.framasoft.org/ploc/phpmyaddressbook/compare/0.9...1.0) (2003-05-09)


### Improvements

- pdf export module has a 2-sided printing capability
- full revision of the code to fulfill the pear coding standards
- using fpdf151
- various improvements



<a name="0.9"></a>
# [0.9](https://git.framasoft.org/ploc/phpmyaddressbook/compare/0.8...0.9) (2003-03-02)


### Features

- ldif export module
- combo box to choose the export format


### Improvements

- search box
- various improvements



<a name="0.8"></a>
# [0.8](https://git.framasoft.org/ploc/phpmyaddressbook/compare/0.7...0.8) (2002-04-05)


### Features

- pdf export module
- text export module


### Improvements

- records are sorted depending on a field
- phpMyAddressbook updated to php4
- various improvements
- print export module removed


### Bug Fixes

- cellphone is correctly set when adding a record



<a name="0.7"></a>
# [0.7](https://git.framasoft.org/ploc/phpmyaddressbook/compare/0.6...0.7) (2001-07-03)


### Improvements

- full revision of the code (optimization, harmonization, comments)
- automatic database structure management
- various improvements



<a name="0.6"></a>
# [0.6](https://git.framasoft.org/ploc/phpmyaddressbook/compare/0.5...0.6) (2001-06-08)


### Features

- print export module


### Improvements

- various improvements



<a name="0.5"></a>
# [0.5](https://git.framasoft.org/ploc/phpmyaddressbook/compare/0.4...0.5) (2001-05-04)


### Improvements

- sql export file is downloaded
- various improvements



<a name="0.4"></a>
# [0.4](https://git.framasoft.org/ploc/phpmyaddressbook/compare/0.3...0.4) (2001-04-22)


### Features

- confirmation before deleting a record



<a name="0.3"></a>
# [0.3](https://git.framasoft.org/ploc/phpmyaddressbook/compare/0.2...0.3) (2001-04-02)


### Features

- record delete feature


### Improvements

- search mode is case insensitive



<a name="0.2"></a>
# [0.2](https://git.framasoft.org/ploc/phpmyaddressbook/compare/0.1...0.2) (2000-12-20)


### Features

- sql export module


### Improvements

- icons



<a name="0.1"></a>
## 0.1 (2000-10-27)


### Features

- initial release
