# phpMyAddressbook

## personal addressbook

Copyright (C) 2000-2022 Ploc

- homepage: <https://framagit.org/ploc/phpmyaddressbook>
- support: [phpmyaddressbook-at-acampado.net](mailto:phpmyaddressbook-at-acampado.net)

### about

- phpMyAddressbook is a personal addressbook using php, fpdf and any pdo supported database engine (currently implemented database engines are sqlite and mysql).
- phpMyAddressbook main language is english but it can be easily translated as it uses gettext. French language package is available and enabled by default.
- phpMyAddressbook is highly customizable as it can potentially handle unlimited number of fields. Many parameters can be set in configuration files.
- phpMyAddressbook can export records to various formats such as pdf booklet, pdf labels, sql, ldif, vcard, csv, zimbra, mab. It can also send basic mass-mailings to get a human validation from your contacts. Besides, phpMyAddressbook can import csv data if the csv file fits the database structure.
- phpMyAddressbook includes icons from [IcoMoon](https://icomoon.io/), [fpdf](http://fpdf.org/) as the pdf generator and [pdf label](http://fpdf.org/fr/script/script29.php) as the pdf label generator.

This is how phpMyAddressbook looks on a desktop screen:

![screenshot of phpMyAddressbook on a desktop](docs/screenshot-desktop.png)

This is how phpMyAddressbook looks on a smartphone screen:

![screenshot of phpMyAddressbook on a smartphone](docs/screenshot-smartphone.png)

### requirements

- a web server
- php 5 (or later) with mbstring and pdo enabled
- a database engine : sqlite or mysql

### installation

- Extract the downloaded archive in a directory of your web server.
- If you want to use apache authentication, rename the file named htaccess to .htaccess and edit the file in order to refer to your own `.htpasswd` file.
- The user configuration file named `user_config.inc.php` allows you to set the behaviour of phpMyAddressbook. Any setting from the main configuration file named `config.inc.php` can be overriden in the user configuration file.  Please read the [MANUAL](docs/MANUAL.md) file for more information.
- Edit the user configuration file named `user_config.inc.php` according to the configuration of your database. Default settings enable an embedded sqlite database to store your data. If you prefer to use mysql, set `$dbEngine` to `mysql` and set the mysql parameters according to your mysql configuration.
- If you set a mysql database, check that the mysql database whose name correponds to `$dbName` already exists on your mysql server and that the mysql user whose name corresponds to `$dbLogin` has write access on it (create, alter, insert, update, delete).
- Open your browser and browse the script named `install.php` (this script creates the table that will hold your data). If your browser displays an error about gettext, check that gettext extension is enabled in your php configuration, or disable gettext in the main configuration file named config.inc.php. If your browser displays an error about the database, check the database settings in the user configuration file and that the table whose name corresponds to `$dbTable` does not already exist in your database.
- If your browser displays that the installation completed, you can start to enter your data.

### todo

- add : various types of content.
- add : multiple search criteria.

### licence

This file is part of phpMyAddressbook.

phpMyAddressbook is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

phpMyAddressbook is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
